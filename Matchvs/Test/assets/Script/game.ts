import RoleCtr from "./RoleCtr";
import { msEngine } from "./MatchEngine";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Game extends cc.Component {

    @property(cc.Prefab)
    rolePrefab: cc.Prefab

    onLoad() {
        cc.director.on('initPlayer', this.initRoles, this);
        cc.director.on('addPlayer', this.addRole, this);
        cc.director.on('playerLeave', this.roleLeave, this);
    }

    start() {

    }

    initRoles(roomUserInfoList: MsRoomUserInfo[]) {
        //init self
        let self = cc.instantiate(this.rolePrefab);
        let roleCtr = self.getComponent(RoleCtr)
        roleCtr.bindUserID(msEngine.userInfo.id);
        self.setPosition(0, -180);
        this.node.addChild(self, 1, msEngine.userInfo.id.toString());

        //init Other
        for (let userInfo of roomUserInfoList) {
            let other = cc.instantiate(this.rolePrefab);
            let otherCtr = other.getComponent(RoleCtr)
            otherCtr.bindUserID(userInfo.userID);
            other.setPosition(0, -180);
            this.node.addChild(other, 1, userInfo.userID.toString());
        }
    }

    addRole(userInfo: MsRoomUserInfo) {
        let other = cc.instantiate(this.rolePrefab);
        let otherCtr = other.getComponent(RoleCtr)
        otherCtr.bindUserID(userInfo.userID);
        other.setPosition(0, -180);
        this.node.addChild(other, 1, userInfo.userID.toString());
    }

    roleLeave(userID) {
        this.node.getChildByName(userID.toString()).removeFromParent(true);
    }
    // update (dt) {}
}
