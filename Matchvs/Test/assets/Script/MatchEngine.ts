class MsEngine {
    private a = console.log(MatchvsEngine);
    private engine = new MatchvsEngine();
    private response = new MatchvsResponse();

    public userInfo: MsRegistRsp = null;
    public init() {
        this.response.initResponse = (status: number) => {
            if (status == 200) {
                //成功
            } else {
                //失败
            }
        }
        this.engine.init(this.response, "Matchvs", "alpha", 216363, "66939fadf549498e83598bcaf4b5ecbe#M", 1.0);
        this.response.frameUpdate = this.frameUpdate.bind(this);
        this.response.sendFrameEventResponse = function(rsp: MsSendFrameEventRsp) {
            if (rsp.mStatus == 200) {

            } else {

            }
        }
        
        cc.director.on('sendEvent', this.sendEvent, this);
        return this;
    }


    public registerUser(isAutoLogin: boolean = false, callback?: (isLoginSuccess: boolean) => void) {
        this.response.registerUserResponse = (userInfo: MsRegistRsp) => {
            if (userInfo.status == 0) {
                this.userInfo = userInfo;
                isAutoLogin && (this.login(userInfo, callback));
            } else {
                //失败
            }
        }
        this.engine.registerUser();
    }

    public login(userInfo: MsRegistRsp, callback?: (isLoginSuccess: boolean) => void) {
        this.response.loginResponse = (loginRsp: MsLoginRsp) => {
            callback && callback(loginRsp.status == 200);
            if (loginRsp.status == 200) {
                //成功

            } else {
                //失败
            }
        }

        this.engine.login(userInfo.id, "1234", '1');
    }

    public joinRoom(callback?: (roomUserInfoList: MsRoomUserInfo[]) => void) {
        console.log('joinRoom')
        let self = this;

        this.response.networkStateNotify = function (netNotify: MsNetworkStateNotify) {
            console.log('玩家网络状况');
            console.log(netNotify);
            cc.director.emit('playerLeave', netNotify.userID);
            switch (netNotify.state) {
                case 1: // 重连
                    break;
                case 2: //重连成功
                    break;
                case 3: // 重连失败
                    break;
            }
        }

        this.response.joinOpenNotify = function (data: MsReopenRoomNotify) {

        }

        this.response.joinOverResponse = function (data: MsJoinOverRsp) {

        }

        this.response.joinOpenResponse = function (data: MsReopenRoomResponse) {

        }

        this.response.leaveRoomNotify = function (data: MsLeaveRoomNotify) {
            console.log('离开房间')
            console.log(data)
        }

        this.response.createRoomResponse = function (rsp: MsCreateRoomRsp) {
            console.log('房间创建成功')
            console.log(rsp);

        }

        this.response.joinRoomNotify = function (roomUserInfo: MsRoomUserInfo) {
            console.log('有玩家加入')
            console.log(roomUserInfo);
            cc.director.emit('addPlayer', roomUserInfo);
        }

        this.response.setFrameSyncResponse = function(rsp: MsSetChannelFrameSyncRsp) {
            if (rsp.status == 200) {
                console.log('帧同步设置成功');
            } else {
                console.log('帧同步设置失败')
                console.log(arguments);
            }
        }

        this.response.setFrameSyncNotify = function(rsp: MVS.MsSetFrameSyncNotify) {
            console.log(rsp)
        }

        this.response.joinRoomResponse = function (status: number, roomUserInfoList: MsRoomUserInfo[], roomInfo: MsRoomInfo) {
            if (status == 200) {
                callback && callback(roomUserInfoList);
            } else {
                MsCreateRoomInfo
                self.engine.createRoom({
                    roomName: 'Fight',
                    maxPlayer: 10,
                    mode: 0,
                    canWatch: 1,
                    visibility: 1,
                    roomProperty: ''
                }, "");
            }
        }

        //this.engine.joinRoom('001', this.userInfo.name);
        this.engine.joinRandomRoom(10, '');
    }

    // 帧同步
    frameUpdate(frameData: MsFrameData) {
        for (let item of frameData.frameItems) {
            let event = JSON.parse(item.cpProto);
            console.log(event);
            cc.director.emit(event.name, event.data);
        }
    }

    sendEvent(event: {name: string, data: {}}) {
        this.engine.sendFrameEvent(JSON.stringify(event))
    }
}
export const msEngine = new MsEngine().init();