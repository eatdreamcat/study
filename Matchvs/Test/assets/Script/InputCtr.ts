import { msEngine } from "./MatchEngine";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class InputCtr extends cc.Component {

    private userID: string = '';
    onLoad() {
        this.userID = msEngine.userInfo.id.toString();
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyPress, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start() {

    }

    onKeyPress(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.d:
                msEngine.sendEvent({
                    name: "move",
                    data: {
                        userID: this.userID,
                        direction: 1
                    }
                });
                break;
            case cc.macro.KEY.a:
                msEngine.sendEvent({
                    name: "move",
                    data: {
                        userID: this.userID,
                        direction: -1
                    }
                })
                break;
            case cc.macro.KEY.j:
                msEngine.sendEvent({
                    name: 'attack',
                    data: {
                        userID: this.userID
                    }
                });
                break;
        }
    }

    onKeyUp(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.d:
            case cc.macro.KEY.a:
            case cc.macro.KEY.j:
                msEngine.sendEvent({
                    name: "idle",
                    data: {
                        userID: this.userID
                    }
                })
                break;
        }
    }
}
