import { msEngine } from "./MatchEngine";
import Game from "./game";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    @property(cc.Label)
    label: cc.Label

    start () {
        msEngine.registerUser(true, function(isLoginSuccess: boolean) {
            if (isLoginSuccess) {
                msEngine.joinRoom((roomUserInfoList: MsRoomUserInfo[]) => {
                    cc.director.loadScene('game', () => {
                        cc.director.emit('initPlayer', roomUserInfoList);
                    });
                });
            } else {
                this.label.string = '登陆失败...'
            }
        }.bind(this));
    }
}
