import { msEngine } from "./MatchEngine";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;


enum ROLE_STATE {
    IDLE,
    WALK,
    RUN,
    HIT,
    ATTACK
}
@ccclass
export default class RoleCtr extends cc.Component {

    speed: number = 2;
    attackTimesPerSec: 0.7;
    _direction: number = 1;
    userID: string = '';

    private state: ROLE_STATE = ROLE_STATE.IDLE;

    @property(cc.Label)
    nameLabel: cc.Label;
    @property(sp.Skeleton)
    spine: sp.Skeleton;


    onLoad() {
        cc.director.on('move', this.move, this);
        cc.director.on('idle', this.idle, this);
        cc.director.on('attack', this.attack, this);
    }

    idle(data: { userID: string }) {
        if (data.userID != this.userID) return;
        this.state = ROLE_STATE.IDLE;
    }

    move(data: { userID: string, direction: number }) {
        if (data.userID != this.userID) return;
        this.direction = data.direction;
        this.state = ROLE_STATE.WALK;
    }


    set direction(direction: number) {
        this._direction = direction;
        this.spine.node.scaleX = direction;
    }
    get direction() {
        return this._direction;
    }

    attack(data: {userID: string}) {
        if (this.userID != data.userID) return;
        if (this.spine.animation == 'attack') return;
        this.spine.animation = 'attack';
        this.spine.loop = false;
        this.state = ROLE_STATE.ATTACK;
       
    }



    bindUserID(userID) {
        this.userID = userID;
        if (userID != msEngine.userInfo.id) {
            this.nameLabel.node.color = cc.color(255, 100, 50);
        } else {
            this.nameLabel.node.color = cc.color(100, 255, 50);
        }
        this.nameLabel.string = userID;
    }

    start() {
        this.spine.defaultAnimation = 'idle';
        this.spine.animation = 'idle';
        this.spine.loop = true;
    }

    update(dt) {
        //if (this.state == ROLE_STATE.ATTACK) return;
        switch (this.state) {
            case ROLE_STATE.IDLE:
                if (this.spine.animation == 'idle') return;
                this.spine.animation = 'idle';
                this.spine.loop = true;
                break;
            case ROLE_STATE.WALK:
                this.node.x += this.direction * this.speed;
                if (this.spine.animation == 'run') return;
                this.spine.animation = 'run';
                this.spine.loop = true;
                break;
        }
    }
}
