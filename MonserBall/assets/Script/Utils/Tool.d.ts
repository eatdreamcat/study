/**
 * 拓展的一些数学方法
 */
interface CMath {
  /**
   * return a new val between max and min
   * @param val
   * @param max
   * @param min
   */
  Clamp(val: number, max: number, min: number);
}
declare var CMath: CMath;
