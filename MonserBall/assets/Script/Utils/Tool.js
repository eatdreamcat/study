/**
 * 插件脚本，可以做一些拓展功能
 */
CMath = {};
CMath.Clamp = function(val, max, min) {
  return Math.max(Math.min(val, max), min);
};
