const { ccclass, property } = cc._decorator;

@ccclass
export default class CameraController extends cc.Component {
  @property({
    type: cc.Float,
    displayName: "镜头缩放最小值"
  })
  minZoomRatio: number = 0.5;

  // TODO  计算逻辑似乎有问题
  @property({
    type: cc.Float,
    displayName: "镜头缩放敏感度"
  })
  zoomRatioSense: number = 1;

  @property({
    displayName: "是否开启镜头缩放"
  })
  isEnableCameraZoom: boolean = true;

  @property(cc.Camera)
  MainCamera: cc.Camera = null;

  private target: cc.Node = null;
  onLoad() {}

  start() {}

  lateUpdate() {
    if (!this.target) return;
    // 移动垂直方向镜头
    this.MainCamera.node.y = this.target.y - cc.winSize.height / 4;

    this.MainCamera.node.y = CMath.Clamp(
      this.MainCamera.node.y,
      2 * cc.winSize.height,
      0
    );

    // 移动水平方向镜头
    if (this.target.x > cc.winSize.width / 4) {
      this.MainCamera.node.x = this.target.x - cc.winSize.width / 4;
    } else if (this.target.x < -cc.winSize.width / 4) {
      this.MainCamera.node.x = this.target.x + cc.winSize.width / 4;
    }

    this.MainCamera.node.x = CMath.Clamp(
      this.MainCamera.node.x,
      cc.winSize.width / 2,
      -cc.winSize.width / 2
    );

    // 镜头缩放
    if (this.isEnableCameraZoom) {
      let ratioX =
        1 - (Math.abs(this.MainCamera.node.x) / cc.winSize.width) * 2;
      let ratioY = 1 - Math.abs(this.MainCamera.node.y) / cc.winSize.height / 2;
      ratioY /= this.zoomRatioSense;
      if (ratioY < this.minZoomRatio) ratioY = this.minZoomRatio;
      ratioY += (1 - ratioX) * (1 - ratioY);

      this.MainCamera.zoomRatio = ratioY;
    }
  }
}
