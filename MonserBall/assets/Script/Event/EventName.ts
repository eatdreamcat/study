/**
 * 全局事件名字定义
 */
export enum GlobalEvent {
  JUMP = "1",
  /** 怪兽攻击 */
  MONSTER_ATTACKED = "2",
  /** 玩家攻击 */
  PLAYER_ATTACK = "3",
  VICTORY = "4",
  DEFEATED = "5",
  UPDATE_BALL_ITEM_NUMBER = "6"
}
