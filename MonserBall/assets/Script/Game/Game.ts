import { TableMgr } from "../TableMgr";

export class Game {
  private static ins: Game;
  public static get inst() {
    return this.ins ? this.ins : (this.ins = new Game());
  }
  private constructor() {}

  public level: number = 5001;
  public health: number = 0;
  public score: number = 0;
  public balls: any = {};
  public curBall: number;

  private reset() {
    this.level = 5001;
    this.health = 0;
    this.score = 0;
    this.balls = {};
  }

  private initBalls() {
    let ballInfo = TableMgr.inst.getMonster_ball_paly_level(this.level).ball;
    for (let ball of ballInfo) {
      this.balls[parseInt(ball.split("|")[0])] = parseInt(ball.split("|")[1]);
    }
  }

  public start() {
    this.reset();
    this.initBalls();
  }

  public changeCurBall(ballID: number) {
    if (this.curBall == ballID) return;
    if (!this.balls[ballID] || this.balls[ballID] <= 0) return;
    this.balls[ballID]--;
    if (this.curBall) this.balls[this.curBall]++;
    this.curBall = ballID;
  }
}
