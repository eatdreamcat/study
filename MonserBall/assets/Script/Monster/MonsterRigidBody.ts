import { GlobalEvent } from "../Event/EventName";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class MonsterRigidBody extends cc.RigidBody {
  /**
   * 攻击力 [0-1]
   */
  @property({
    type: cc.Float,
    displayName: "攻击力",
    tooltip: "攻击力，0-1之间"
  })
  attack: number = 0.0;

  /**
   * 攻击间隔每秒攻击次数
   */
  @property({
    type: cc.Float,
    displayName: "攻击速度",
    tooltip: "每秒攻击次数"
  })
  attackSpeed: number = 1;

  /**
   * 生命值
   */
  @property({
    type: cc.Float,
    displayName: "生命值"
  })
  health: number = 100;

  /** 攻击一次的耗时 */
  private secsPerAttack: number = 100;
  private time: number = 0;
  onLoad() {
    this.secsPerAttack = 1 / this.attackSpeed;
    this.linearVelocity = cc.v2(500, 0);
  }

  update(dt) {
    this.time += dt;
    if (this.time > this.secsPerAttack) {
      this.time = 0;
      cc.director.emit(GlobalEvent.MONSTER_ATTACKED, this.attack);
    }
  }

  onBeginContact(
    contact: cc.PhysicsContact,
    selfCollider: cc.PhysicsCollider,
    otherCollider: cc.PhysicsCollider
  ) {
    if (otherCollider.node.group == "wall") {
      this.node.scaleX = -this.node.scaleX;
      this.linearVelocity = cc.v2(500, 0);
    }
  }

  onEndContact(
    contact: cc.PhysicsContact,
    selfCollider: cc.PhysicsCollider,
    otherCollider: cc.PhysicsCollider
  ) {}
}
