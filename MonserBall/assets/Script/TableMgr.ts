import {
  Monster_ball__monster,
  Monster_ball_ball,
  Monster_ball_paly_level,
  Monster_ball_prop,
  Monster_ball_scene
} from "./table";
/**
 * json数据管理
 */
export class TableMgr {
  private static ins: TableMgr;
  public static get inst() {
    return this.ins ? this.ins : (this.ins = new TableMgr());
  }
  private constructor() {}
  private total: number = 0;
  private complete: number = 0;
  private completeCallback: () => void;
  private progressCallback: (progress: number) => void;
  /**
   *
   * @param url json 路径
   * @param complete
   * @param progress
   */
  startLoad(
    url: string,
    complete: () => void,
    progress?: (progress: number) => void
  ) {
    this.completeCallback = complete;
    this.progressCallback = progress;
    let self = this;
    cc.loader.loadRes(
      url
        .trim()
        .split("/")
        .join("") + "/file_list",
      cc.JsonAsset,
      function(err, JsonAsset: cc.JsonAsset) {
        if (err) {
          console.error(err);
        } else {
          this.total = JsonAsset.json.length;
          for (let jsonFile of JsonAsset.json) {
            self.loadJson(
              url
                .trim()
                .split("/")
                .join("") +
                "/" +
                jsonFile.replace(".json", "")
            );
          }
        }
      }.bind(this)
    );
  }
  private loadJson(url) {
    console.log("start load:" + url);
    let self = this;
    cc.loader.loadRes(
      url,
      cc.JsonAsset,
      function(err, JsonAsset: cc.JsonAsset) {
        if (err) {
          console.error(err);
        } else {
          for (let json of JsonAsset.json) {
            self[JsonAsset.name][json["ID"]] = json;
          }
          self.completeLoad();
        }
      }.bind(this)
    );
  }
  private completeLoad() {
    this.complete++;
    if (this.complete >= this.total) {
      if (this.completeCallback) this.completeCallback();
    }
  }
  private Monster_ball__monster: any = {};
  private Monster_ball_ball: any = {};
  private Monster_ball_paly_level: any = {};
  private Monster_ball_prop: any = {};
  private Monster_ball_scene: any = {};
  public getMonster_ball__monster(key: string | number): Monster_ball__monster {
    if (this.Monster_ball__monster[key]) {
      return this.Monster_ball__monster[key];
    } else {
      console.error("Monster_ball__monster 不存key：" + key);
      return null;
    }
  }
  public getMonster_ball_ball(key: string | number): Monster_ball_ball {
    if (this.Monster_ball_ball[key]) {
      return this.Monster_ball_ball[key];
    } else {
      console.error("Monster_ball_ball 不存key：" + key);
      return null;
    }
  }
  public getMonster_ball_paly_level(
    key: string | number
  ): Monster_ball_paly_level {
    if (this.Monster_ball_paly_level[key]) {
      return this.Monster_ball_paly_level[key];
    } else {
      console.error("Monster_ball_paly_level 不存key：" + key);
      return null;
    }
  }
  public getMonster_ball_prop(key: string | number): Monster_ball_prop {
    if (this.Monster_ball_prop[key]) {
      return this.Monster_ball_prop[key];
    } else {
      console.error("Monster_ball_prop 不存key：" + key);
      return null;
    }
  }
  public getMonster_ball_scene(key: string | number): Monster_ball_scene {
    if (this.Monster_ball_scene[key]) {
      return this.Monster_ball_scene[key];
    } else {
      console.error("Monster_ball_scene 不存key：" + key);
      return null;
    }
  }
}
