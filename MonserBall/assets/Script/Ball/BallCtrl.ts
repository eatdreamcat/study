import { GlobalEvent } from "../Event/EventName";
import { JUMP_DIR, PhysicConf, Config } from "../Config/Config";
import { Monster_ball_ball_form } from "../table";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class BallCtrl extends cc.RigidBody {
  // LIFE-CYCLE CALLBACKS:
  /** 球的类型 */
  private form: Monster_ball_ball_form;
  /** 持续时间 */
  private continue: number;
  /** 使用寿命 */
  private lifeTime: number;
  /** 攻击力 */
  private attack: number;
  onLoad() {}

  start() {
    cc.director.on(GlobalEvent.JUMP, this.onTouch.bind(this), this);
    this.node.parent.zIndex = -1;
  }

  onTouch(direction: JUMP_DIR) {
    if (direction == JUMP_DIR.LEFT) {
      this.angularVelocity = -PhysicConf.angularVelocity;
      this.linearVelocity = PhysicConf.linearVelocity.neg();
    } else {
      this.linearVelocity = PhysicConf.linearVelocity;
      this.angularVelocity = PhysicConf.angularVelocity;
    }
    this.applyForceToCenter(PhysicConf.applyForce, true);
  }
}
