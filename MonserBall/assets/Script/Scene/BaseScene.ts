const { ccclass, property } = cc._decorator;

/**
 * 所有场景的Script Component 都要继承这个
 * 包含基础的UI界面管理
 */
@ccclass
export default class BaseScene extends cc.Component {
  protected showUI(url: string) {
    let self = this;
    let nodeName = url.split("/").join("_");
    if (this.node.getChildByName(nodeName)) {
      console.warn("界面已显示:" + url);
    } else {
      cc.loader.loadRes(url, cc.Prefab, (err, prefab: cc.Prefab) => {
        if (err) {
          console.error(err);
        } else {
          let prefabNode = cc.instantiate(prefab);
          prefabNode.setPosition(0, 0);
          self.node.addChild(prefabNode, 1, nodeName);
        }
      });
    }
  }

  protected closeUI(url, release: boolean = false) {
    let nodeName = url.split("/").join("_");
    let child = this.node.getChildByName(nodeName);
    if (child) {
      child.removeFromParent(true);
      if (release) {
        cc.loader.releaseRes(url);
      }
    }
  }
}
