import BaseScene from "./BaseScene";
import { HashMap } from "../Utils/HashMap";
import { Config } from "../Config/Config";
import { TableMgr } from "../TableMgr";

const { ccclass, property } = cc._decorator;

/** 加载的步骤 */
export enum LOAD_STEP {
  /** 初始化 */
  READY = 2 << 0,
  /** 初始化 */
  INIT = 2 << 1,
  /** 注册 */
  REGISTER = 2 << 2,
  /** 登录 */
  LOGIN = 2 << 3,
  /** 匹配 */
  MATCH = 2 << 4,
  /** 加载表 */
  JSON_PARSE = 2 << 5,
  /** 动画播放完成 */
  ANIMATION_DONE = 2 << 6,
  /** 完成 */
  DONE = LOAD_STEP.READY |
    LOAD_STEP.INIT |
    LOAD_STEP.REGISTER |
    LOAD_STEP.LOGIN |
    LOAD_STEP.MATCH |
    LOAD_STEP.JSON_PARSE |
    LOAD_STEP.ANIMATION_DONE
}

@ccclass
export default class WelcomeScene extends BaseScene {
  /** 下一个场景名字 */
  @property({
    displayName: "游戏场景名",
    tooltip: "默认进入Game场景，如果需要指定场景，可以指定"
  })
  nextSceneName: string = "Game";

  /** 进入下一个场景前需要执行的callbacks */
  private _callbacks: HashMap<LOAD_STEP, Function> = new HashMap();
  /** 当前的步骤 */
  private _step: LOAD_STEP = LOAD_STEP.READY;
  private defaultAnimation: cc.Animation;
  onLoad() {
    this.defaultAnimation = this.node.getComponent(cc.Animation);
    if (this.defaultAnimation) {
      this.defaultAnimation.once(
        cc.Animation.EventType.FINISHED,
        this.animationDone,
        this
      );
    } else {
      this.nextStep(LOAD_STEP.ANIMATION_DONE);
    }

    if (Config.isMultiPlayer) {
    } else {
      this.nextStep(LOAD_STEP.LOGIN);
      this.nextStep(LOAD_STEP.MATCH);
      this.nextStep(LOAD_STEP.REGISTER);
      this.nextStep(LOAD_STEP.INIT);
    }

    TableMgr.inst.startLoad("json/", () => {
      this.nextStep(LOAD_STEP.JSON_PARSE);
    });
  }

  animationDone() {
    this.defaultAnimation.off(cc.Animation.EventType.FINISHED);
    this.nextStep(LOAD_STEP.ANIMATION_DONE);
  }

  /** 注册每一步的回调 */
  public registerStep(
    step: LOAD_STEP,
    callback: (curStep: LOAD_STEP) => boolean
  ) {
    this._callbacks.add(step, callback);
  }

  /**
   * 下一步
   */
  private nextStep(loadStep: LOAD_STEP) {
    this._step |= loadStep;
    console.log("CUR STEP:" + LOAD_STEP[loadStep]);
    if (this._step >= LOAD_STEP.DONE) {
      cc.director.loadScene(this.nextSceneName);
    } else {
      if (loadStep == LOAD_STEP.ANIMATION_DONE) {
        if (this.defaultAnimation) {
          this._step ^= LOAD_STEP.ANIMATION_DONE;
          this.defaultAnimation.once(
            cc.Animation.EventType.FINISHED,
            this.animationDone,
            this
          );
          this.defaultAnimation.play();
        }
      }
    }
  }
}
