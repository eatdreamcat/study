import { GlobalEvent } from "../Event/EventName";
import { JUMP_DIR } from "../Config/Config";
import { TableMgr } from "../TableMgr";
import { Game } from "../Game/Game";
import BallItem from "./BallItem";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
/**
 * 主UI界面
 */
@ccclass
export default class MainUI extends cc.Component {
  @property(cc.ProgressBar)
  HpProgressBar: cc.ProgressBar = null;

  @property(cc.ScrollView)
  BallScrollView: cc.ScrollView = null;

  onLoad() {
    this.HpProgressBar.progress = 1;
    this.HpProgressBar.barSprite.node.color = cc.color(34, 73, 24);
    cc.director.on(
      GlobalEvent.MONSTER_ATTACKED,
      this.attacked.bind(this),
      this
    );

    this.node.on(cc.Node.EventType.TOUCH_START, this.onTouched.bind(this));
    this.updateBallList();
  }

  private updateBallList() {
    let levelData = TableMgr.inst.getMonster_ball_paly_level(Game.inst.level);
    let self = this;
    cc.loader.loadRes(
      "Prefabs/BallItem",
      cc.Prefab,
      (err, prefab: cc.Prefab) => {
        if (err) {
          console.error(err);
        } else {
          for (let ballInfo of levelData.ball) {
            if (ballInfo == "") continue;
            let infoSplit = ballInfo.split("|");
            let ballItem = cc.instantiate(prefab);
            ballItem
              .getComponent(BallItem)
              .updateBallInfo(parseInt(infoSplit[0]));
            ballItem.y = -50;
            self.BallScrollView.content.addChild(ballItem);
          }
        }
      }
    );
  }

  onTouched(touch: cc.Event.EventTouch) {
    cc.director.emit(
      GlobalEvent.JUMP,
      touch.getLocationX() > cc.winSize.width / 2
        ? JUMP_DIR.RIGHT
        : JUMP_DIR.LEFT
    );
  }

  attacked(damage: number) {
    this.HpProgressBar.progress -= damage;
    if (this.HpProgressBar.progress > 0.4) {
      this.HpProgressBar.barSprite.node.color = cc.color(34, 73, 24);
    } else {
      this.HpProgressBar.barSprite.node.color = cc.color(185, 34, 8);
    }

    if (this.HpProgressBar.progress <= 0) {
      cc.director.emit(GlobalEvent.DEFEATED);
      cc.director.loadScene("Over", (err, overScene: cc.Scene) => {});
    }
  }

  start() {}

  update(dt) {}
}
