import { TableMgr } from "../TableMgr";
import { Monster_ball_ball_form } from "../table";
import { Game } from "../Game/Game";
import { GlobalEvent } from "../Event/EventName";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class BallItem extends cc.Component {
  @property(cc.Sprite)
  BallIcon: cc.Sprite = null;

  @property(cc.Label)
  Number: cc.Label = null;

  private ballID: number;
  private ballNumber: number;

  onLoad() {
    cc.director.on(
      GlobalEvent.UPDATE_BALL_ITEM_NUMBER,
      this.updateBallNumber,
      this
    );
    this.node.on(cc.Node.EventType.TOUCH_END, this.selectBall, this);
    this.getComponent(cc.Button).enableAutoGrayEffect = true;
  }

  start() {}

  selectBall() {
    Game.inst.changeCurBall(this.ballID);
    cc.director.emit(GlobalEvent.UPDATE_BALL_ITEM_NUMBER);
  }

  updateBallNumber() {
    this.ballNumber = Game.inst.balls[this.ballID];
    this.Number.string = "X" + this.ballNumber;
    this.getComponent(cc.Button).interactable = this.ballNumber > 0;
  }

  updateBallInfo(ballID: number) {
    this.ballID = ballID;
    this.updateBallNumber();
    let ballInfo = TableMgr.inst.getMonster_ball_ball(ballID);
    switch (ballInfo.form) {
      case Monster_ball_ball_form.DianJiMaBi:
        this.node.color = cc.Color.GREEN;
        break;
      case Monster_ball_ball_form.PuTong:
        this.node.color = cc.Color.BLUE;
        break;
      case Monster_ball_ball_form.ZhaDan:
        this.node.color = cc.Color.RED;
        break;
    }
  }

  // update (dt) {}
}
