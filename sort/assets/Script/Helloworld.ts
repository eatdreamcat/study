const { ccclass, property } = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    @property
    arrayNumber: number[] = [];

    start() {
        // init logic
        this.label.string = this.text;
        
        for (let i = 0; i <= 5000; ++i) {
            this.arrayNumber.push(Math.round(Math.random()*5000*Math.random()));
        }
        // 冒泡
        this.bubblingSort(this.arrayNumber.concat(), this.arrayNumber.length);
        // 选择
        this.selectSort(this.arrayNumber.concat(), this.arrayNumber.length);
        // 插入
        this.insertSort(this.arrayNumber.concat(), this.arrayNumber.length);
        // 希尔
        //this.shellSort(this.arrayNumber.concat(), this.arrayNumber.length);
        // 快排
        this.quickSort(this.arrayNumber.concat(), this.arrayNumber.length);
        // 归并排序
        this.mergeSort(this.arrayNumber.concat(), this.arrayNumber.length);
        // 堆排序
        this.heapSort(this.arrayNumber.concat(), this.arrayNumber.length);
    }

    /** 冒泡排序 */
    bubblingSort(array: Array<number>, length: number, log: boolean = true) {
        let now = Date.now();
        for (let i = 0; i < length; ++i) {
            for (let j = i + 1; j < length; ++j) {
                if (array[i] < array[j]) [array[i], array[j]] = [array[j], array[i]];
            }
        }
        if (log) {
            console.warn('冒泡排序耗时:' + (Date.now() - now) + 'ms');
            console.log(array.join(',  '));
        }
        return array;
    }

    /** 选择排序 */
    selectSort(array: Array<number>, length: number, log: boolean = true) {
        let now = Date.now();
        for (let i = 0; i < length; ++i) {
            let selected = i;
            for (let j = i + 1; j < length; ++j) {
                if (array[selected] < array[j]) selected = j;
            }
            [array[i], array[selected]] = [array[selected], array[i]];
        }
        if (log) {
            console.warn('选择排序耗时:' + (Date.now() - now) + 'ms');
            console.log(array.join(',  '));
        }
        return array;
    }

    /** 插入排序 */
    insertSort(array: Array<number>, length: number, log: boolean = true) {
        let now = Date.now();
        for (let i = 0; i < length; ++i) {
            for (let j = i + 1; j > 0; --j) {
                if (array[j] > array[j - 1]) [array[j - 1], array[j]] = [array[j], array[j - 1]];
                else break;
            }
        }
        if (log) {
            console.warn('插入排序耗时:' + (Date.now() - now) + 'ms');
            console.log(array.join(',  '));
        }
        return array;
    }

    /** 希尔排序 TODO 有bug*/
    shellSort(array: Array<number>, length: number, log: boolean = true) {
        let now = Date.now();
        let incre = Math.ceil(length / 2);
        while(incre > 1) {
            for (let i = 0; i < incre; ++ i) {
                for(let j = i + incre; j < length; j +=incre) {
                    for(let k = j; k > i; k -= incre ) {
                        if (array[k] > array[k - incre]) [array[k], array[k - incre]] = [array[k - incre], array[k]];
                        else break;
                    }
                }
            }

            incre = Math.ceil(incre / 2);
        }

        if (log) {
            console.warn('希尔排序耗时:' + (Date.now() - now) + 'ms');
            console.log(array.join(',  '));
        }
        return array;
    }

    /** 快速排序 */
    quickSort(array: Array<number>, length: number, log: boolean = true) {

        function quick(arr: Array<number>, start: number, end: number) {
            if (start >= end) return;

            // 取第一个元素作为基准
            let index = arr[end];
            let i = start, j = end;

            while(i < j) {
                // 向右寻找一个小于index的数
                while( i < j && arr[i] > index) ++i;
                if (i < j) arr[j--] = arr[i];

                // 向左寻找一个大于index的数
                while( i < j && arr[j] <= index) --j;
                if (i < j) arr[i++] = arr[j];
            }

            arr[i] = index;
            quick(arr, start, j - 1);
            quick(arr, j + 1, end);
        }

        let now = Date.now();
        quick(array, 0, length - 1);
        if (log) {
            console.warn('快速排序耗时:' + (Date.now() - now) + 'ms');
            console.log(array.join(',  '));
        }
        return array;
    }

    /** 归并排序 */
    mergeSort(array: Array<number>, length: number, log: boolean = true) {

        // 合并两个有序的数组
        function merge_array(arr: Array<number>, first: number, end: number, middle: number, temp: Array<number>) {
            let i = first, iend = middle;
            let j = middle + 1, jend = end;
            let index = 0;
            // 两个有序数组合并
            while(i <= iend && j <= jend) {
                if (arr[i] > arr[j]) {
                    temp[index ++] = arr[i++];
                } else {
                    temp[index ++] = arr[j++];
                }
            }

            //前半段数组还有未合并完成的
            while(i <= iend) {
                temp[index ++] = arr[i++];
            }

            //后半段数组还有未合并完成的
            while(j <= jend) {
                temp[index ++] = arr[j++];
            }
            
            //将排序好的数组存回原数组
            for (let ii = 0; ii < index; ++ii)
                arr[first + ii] = temp[ii];
        }

        // 将数组拆分并且合并排序
        function merge_sort(arr: Array<number>, first: number, last: number, temp: Array<number>) {
            if (first < last) {
                let middle = Math.floor((first + last) / 2);
                merge_sort(arr, first, middle, temp);
                merge_sort(arr, middle + 1, last, temp);
                merge_array(arr, first, last, middle, temp);
            }
        }


        let now = Date.now();
        let temp = [];
        merge_sort(array, 0, array.length - 1, temp);
        if (log) {
            console.warn('归并排序耗时:' + (Date.now() - now) + 'ms');
            console.log(array.join(',  '));
        }
        return array;
    }

    /** 堆排序 */
    heapSort(array: Array<number>, length: number, log: boolean = true) {
        let now = Date.now();
        
        if (log) {
            console.warn('堆排序耗时:' + (Date.now() - now) + 'ms');
            console.log(array.join(',  '));
        }
        return array;
    }
    
}
