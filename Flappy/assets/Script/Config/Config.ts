/**
 * 游戏配置
 */
export const Config = {
  GameID: 216437,
  AppKey: "bc7b7fd603714d888deff0986e24f742#C",
  Secret: "6f86c9c856504b5d917dd06585553a3f",
  CocosAppID: "619821021",
  channel: "Matchvs",
  platform: "alpha",
  gameVersion: 1.0,
  DeviceID: "1",
  MaxPlayer: 2,
  /** 是否是多人游戏 */
  isMultiPlayer: false
};

/**
 * 物理相关配置
 */
export const PhysicConf = {
  enabled: true,
  gravity: cc.v2(0, -400),
  initPos: cc.v2(-400, 0),
  debugDrawFlags: 0,
  /** 小球角速度 */
  angularVelocity: -60,
  /** 速度 */
  linearVelocity: cc.v2(300, 300)
};
