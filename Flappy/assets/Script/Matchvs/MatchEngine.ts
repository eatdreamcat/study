import { ErrMsg, ResStatusMsg } from "./MsgCode";

export class MatchEngine {
  private static _ins: MatchEngine = null;
  public static get inst() {
    return this._ins ? this._ins : (this._ins = new MatchEngine());
  }

  private static Engine = new MatchvsEngine();
  private static Response = new MatchvsResponse();

  /**
   * 回调执行环境
   */
  private context: any = null;
  /** 玩家用户信息 */
  private userInfo: MsRegistRsp = null;
  private constructor() {
    // init respoonse callback
    MatchEngine.Response.initResponse = this.initResponse.bind(this);
    MatchEngine.Response.registerUserResponse = this.registerUserResponse.bind(
      this
    );
    MatchEngine.Response.loginResponse = this.loginResponse.bind(this);
    MatchEngine.Response.logoutResponse = this.logoutResponse.bind(this);
    MatchEngine.Response.createRoomResponse = this.createRoomResponse.bind(
      this
    );
    MatchEngine.Response.joinRoomResponse = this.joinRoomResponse.bind(this);
    MatchEngine.Response.joinRoomNotify = this.joinRoomNotify.bind(this);
    MatchEngine.Response.joinOverResponse = this.joinOverResponse.bind(this);
    MatchEngine.Response.joinOverNotify = this.joinOverNotify.bind(this);
    MatchEngine.Response.leaveRoomNotify = this.leaveRoomNotify.bind(this);
    MatchEngine.Response.leaveRoomResponse = this.leaveRoomResponse.bind(this);
    MatchEngine.Response.getRoomListExResponse = this.getRoomListExResponse.bind(
      this
    );
  }

  private logErrMsg(ErrMsg: any, errCode: number, msg: string = "") {
    if (ErrMsg[errCode.toString()]) {
      console.log(msg + ErrMsg[errCode.toString()]);
    } else {
      console.log("unknow err code:" + errCode);
    }
  }

  /**
   * 绑定回调环境
   * @param context
   */
  public bindContext(context: any) {
    this.context = context;
  }

  public unbindContext() {
    this.context = null;
  }

  /**
   * 反初始化
   */
  uninit() {
    MatchEngine.Engine.uninit();
  }
  /**
   * 初始化matchvs
   * @param pChanel 渠道，固定值"Matchvs"
   * @param pPlatform 平台，选择测试(alpha)or正式环境(release)
   * @param gameID
   * @param appKey
   * @param gameVersion 游戏版本，自定义，用于隔离匹配空间
   * @param threshold 延迟容忍,在有多个节点的情况下使用，如果使用默认节点可以不传该值
   */
  init(
    pChanel: string,
    pPlatform: string,
    gameID: number,
    appKey: string,
    gameVersion: number,
    threshold?: number
  ): boolean {
    let res = MatchEngine.Engine.init(
      MatchEngine.Response,
      pChanel,
      pPlatform,
      gameID,
      appKey,
      gameVersion,
      threshold
    );
    this.logErrMsg(ErrMsg, res, "init:");
    return res === 0;
  }

  /**
   * 初始化response回调
   * @param status
   */
  private initResponse(status: number) {
    if (status == 200) {
      console.log("response init success");
    } else {
      console.error("response init fail");
    }
    console.log(this.context);
    if (this.context && this.context.initResponse) {
      this.context.initResponse(status);
    }
  }

  public getUserInfo(): MsRegistRsp {
    return this.userInfo;
  }
  /**
   * 注册用户
   * @returns {number} 0-接口调用成功
   */
  public registerUser(): number {
    let res = MatchEngine.Engine.registerUser();
    this.logErrMsg(ErrMsg, res, "register :");
    return res;
  }

  private registerUserResponse(userInfo: MsRegistRsp) {
    this.userInfo = userInfo;
    this.logErrMsg(ResStatusMsg, userInfo.status, "register user response:");
    if (this.context && this.context.registerUserResponse) {
      this.context.registerUserResponse(userInfo);
    }
  }

  /**
   * 登录
   * @param {number} pUserID 用户ID，该值必须使用 registerUser接口回调的用户ID
   * @param {string} pToken  用户验证字段，用于验证 userID
   * @param pDeviceID             用于区分 用户在不同设备登录情况，用户只能在一个设备登录，默认填1，如果允许一个设备登录就要开发者
   *                              自定义唯一值，或者获取 设备ID值
   * @param {number} nodeID 节点ID，如果不传就使用默认节点，如果传了的话需要同时，init的时候设置threshold参数
   * @returns {number}
   */
  public login(
    userID: number,
    token: string,
    deviceID: string,
    nodeID?: number
  ): number {
    let res = MatchEngine.Engine.login(userID, token, deviceID, nodeID);
    this.logErrMsg(ErrMsg, res, "login:");
    return res;
  }

  private loginResponse(login: MsLoginRsp) {
    this.logErrMsg(ResStatusMsg, login.status, "loginResponse:");
    if (this.context && this.context.loginResponse) {
      this.context.loginResponse(login);
    }
  }

  /**
   * 退出登录
   * @param {number} cpProto 开发者自定义数据
   * @returns {number}
   */
  public logout(cpProto: string): number {
    let res = MatchEngine.Engine.logout(cpProto);
    this.logErrMsg(ErrMsg, res, "logout:");
    return res;
  }

  private logoutResponse(status: number) {
    this.logErrMsg(ResStatusMsg, status, "logoutResponse:");
    if (this.context && this.context.loginResponse) {
      this.context.logoutResponse(status);
    }
  }

  /**
   * 随机加入房间
   * @param {number} maxPlayer 房间能容纳的最多人数，该值最大不能超过20
   * @param {string} userProfile 加入房间附带消息，由用户自己定义，比如：头像，分数等
   * @returns {number}
   */
  joinRandomRoom(maxPlayer: number, userProfile: string): number {
    let res = MatchEngine.Engine.joinRandomRoom(maxPlayer, userProfile);
    this.logErrMsg(ErrMsg, res, "join random room:");
    return res;
  }

  /**
   * 属性加入房间，这个属性跟 createRoom 接口传入的属性是不同的，该接口是指，使用开发者自己定义不同的
   * 属性字段由服务器该属性的房间，调用该接口成功后，就在房间中啦；比如：选择不同的地图加入房间，地图相同的人就会加入到
   * 同一个房间。matchinfo 类型的 tags 是 {key=value,key1=value2} 类型，可以指定多个属性。
   * @param {MsMatchInfo} matchinfo 匹配属性规则类型
   * @param {string} userProfile 加入房间附带消息，由用户自己定义，比如：头像，分数等
   * @param {MVS.MsWatchSet} watchSet matchvsinfo 中的 canWatch设置为1(可观战),需要设置这个参数
   * @returns {number}
   */
  joinRoomWithProperties(
    matchinfo: MsMatchInfo,
    userProfile: string,
    watchSet?: MVS.MsWatchSet
  ): number {
    let res = MatchEngine.Engine.joinRoomWithProperties(
      matchinfo,
      userProfile,
      watchSet
    );
    this.logErrMsg(ErrMsg, res, "join room with properties:");
    return res;
  }

  /**
   * 加入指定房间，通过rooID 加入到指定的房间
   * @param {string} roomID  要加入的房间ID
   * @param {string} userProfile 开发者自定义数据，可以比如：用户头像，分数等
   * @param {boolean} isReconnect 是否是断线重连加入，默认false
   * @returns {number}
   */
  joinRoom(
    roomID: string,
    userProfile: string,
    isReconnect: boolean = false
  ): number {
    let res = MatchEngine.Engine.joinRoom(roomID, userProfile, isReconnect);
    this.logErrMsg(ErrMsg, res, "join room:");
    return res;
  }

  /**
   * 开发者调用此接口主动创建房间，房间参数由开发者自己定义。
   * @param {MsCreateRoomInfo} createRoomInfo 房间信息
   * @param {string} userProfile 附带数据，默认指定 ""
   * @param {MVS.MsWatchSet} watchSet 可选参数，指定房间观战参数
   * @returns {number}
   */
  createRoom(
    createRoomInfo: MsCreateRoomInfo,
    userProfile: string,
    watchSet?: MVS.MsWatchSet
  ): number {
    let res = MatchEngine.Engine.createRoom(
      createRoomInfo,
      userProfile,
      watchSet
    );
    this.logErrMsg(ErrMsg, res, "create room:");
    return res;
  }

  private createRoomResponse(rsp: MsCreateRoomRsp) {
    this.logErrMsg(ResStatusMsg, rsp.status, "create room response:");
    if (this.context && this.context.createRoomResponse) {
      this.context.createRoomResponse(rsp);
    }
  }

  private joinRoomResponse(
    status: number,
    roomUserInfoList: Array<MsRoomUserInfo>,
    roomInfo: MsRoomInfo
  ) {
    this.logErrMsg(ResStatusMsg, status, "joinRoom response:");
    if (this.context && this.context.joinRoomResponse) {
      this.context.joinRoomResponse(status, roomUserInfoList, roomInfo);
    }
  }

  private joinRoomNotify(roomUserInfo: MsRoomUserInfo) {
    console.log("join room notify");
    if (this.context && this.context.joinRoomNotify) {
      this.context.joinRoomNotify(roomUserInfo);
    }
  }

  /**
   * 房间内任何人都可以调用禁止加入房间接口，具体的有开发者自己控制
   * @param {string} cpProto 禁止加入房间附带的数据
   * @returns {number}
   */
  public joinOver(cpProto: string): number {
    let res = MatchEngine.Engine.joinOver(cpProto);
    this.logErrMsg(ErrMsg, res, "join over:");
    return res;
  }

  private joinOverResponse(rsp: MsJoinOverRsp) {
    this.logErrMsg(ResStatusMsg, rsp.status, "join over:");
    if (this.context && this.context.joinOverResponse) {
      this.context.joinOverResponse(rsp);
    }
  }

  private joinOverNotify(notifyInfo: MsJoinOverNotifyInfo) {
    console.log("joinOverNotify");
    if (this.context && this.context.joinOverNotify) {
      this.context.joinOverNotify(notifyInfo);
    }
  }

  public leaveRoom(cpProto: string): number {
    let res = MatchEngine.Engine.leaveRoom(cpProto);
    this.logErrMsg(ErrMsg, res, "leave room:");
    return res;
  }

  private leaveRoomResponse(rsp: MsLeaveRoomRsp) {
    this.logErrMsg(ResStatusMsg, rsp.status, "leave room response:");
    if (this.context && this.context.leaveRoomResponse) {
      this.context.leaveRoomResponse(rsp);
    }
  }

  private leaveRoomNotify(leaveRoomInfo: MsLeaveRoomNotify) {
    console.log("leaveRoomNotify");
    if (this.context && this.context.leaveRoomNotify) {
      this.context.leaveRoomNotify(leaveRoomInfo);
    }
  }

  /**
   * 获取房间列表
   * @param filter
   */
  public getRoomListEx(filter: MsRoomFilterEx) {
    MatchEngine.Engine.getRoomListEx(filter);
  }

  private getRoomListExResponse(rsp: MsGetRoomListExRsp) {
    this.logErrMsg(ResStatusMsg, rsp.status, "getRoomListResponse:");
    if (this.context && this.context.getRoomListExResponse) {
      this.context.getRoomListExResponse(rsp);
    }
  }
}
