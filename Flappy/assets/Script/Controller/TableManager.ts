export class TableManager {
    private static ins: TableManager;
    public static get inst() {
        return this.ins ? this.ins : this.ins = new TableManager();
    }

    private constructor() {}

    private total: number = 0;
    private complete: number = 0;

    private completeCallback: () => void;
    private progressCallback: (progress: number) => void;
    startLoad(complete: () => void, progress?: (progress:number) => void) {
        this.completeCallback = complete;
        this.progressCallback = progress;
        cc.loader.loadRes("json/file_list", cc.JsonAsset, function(err, JsonAsset: cc.JsonAsset) {
            if (err) {
                console.error(err);
            } else {
                this.total = JsonAsset.json.length;
                for (let jsonFile of JsonAsset.json) {
                    this.loadJson("json/" + jsonFile.replace(".json", ""));
                }
            }
        }.bind(this));
    }

    private loadJson(url) {
        console.log("start load:" + url);
        cc.loader.loadRes(url, cc.JsonAsset, function(err, JsonAsset: cc.JsonAsset) {
            if (err) {
                console.error(err);
            } else {
                this.completeLoad();
            }
        }.bind(this));
    }

    private completeLoad() {
        this.complete ++;
        if (this.complete >= this.total) {
            if (this.completeCallback) this.completeCallback();
        }
    }
}