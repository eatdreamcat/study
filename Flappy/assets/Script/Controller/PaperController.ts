import { GlobalEvent } from "../Event/EventName";
import { PhysicConf } from "../Config/Config";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class PaperController extends cc.Component {

    @property(cc.Prefab)
    paperNode: cc.Prefab = null;

    @property(cc.Node)
    player: cc.Node = null;

    private paperNodeCount: number = 0;
    onLoad() {
        this.addPaper();
    }

    start () {
        
    }

    addPaper() {
        let paper = cc.instantiate(this.paperNode);
        paper.setPosition(this.paperNodeCount * cc.winSize.width * 2, 0);
        this.node.addChild(paper, 1, "paper" + this.paperNodeCount);
        this.paperNodeCount ++;
        console.log("paper count:" + this.paperNodeCount);
    }
    update (dt) {
        if (this.player.x > cc.winSize.width * this.paperNodeCount + PhysicConf.initPos.x) {
            this.addPaper();
        }
    }
}
