import { GlobalEvent } from "../Event/EventName";

const { ccclass, property } = cc._decorator;

/**
 * 玩家控制输入
 */
@ccclass
export default class InputController extends cc.Component {
  // LIFE-CYCLE CALLBACKS:
  /**触摸检测节点 */
  @property({
    type: cc.Node,
    displayName: "触摸检测节点",
    tooltip: "用作全屏的触摸检测"
  })
  touchInputNode: cc.Node = null;

  onLoad() {
    this.touchInputNode.on(
      cc.Node.EventType.TOUCH_START,
      this.onTouched.bind(this)
    );
  }

  onTouched(touch: cc.Event.EventTouch) {
    cc.director.emit(GlobalEvent.JUMP);
  }

  start() {}

  update(dt) {}
}
