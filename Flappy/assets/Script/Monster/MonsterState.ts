import { HashMap } from "../Utils/HashMap";

/**
 * 怪兽的各种状态
 * 无敌，虚弱，狂躁等
 */
export enum MonsterStateType {}
/**
 * 怪兽的状态
 */
class MonsterState {
  private state: MonsterStateType;

  public getState() {
    return this.state;
  }
  public setState(newState: MonsterStateType) {
    this.state = newState;
  }
}

/**
 * 所有怪兽state的获取]
 * 每个怪兽的撞击点都有独立的状态
 */
export class MonsterStatePool {
  private static _ins: MonsterStatePool;
  public static get inst() {
    return this._ins ? this._ins : (this._ins = new MonsterStatePool());
  }

  private stateHash: HashMap<string, MonsterState> = new HashMap();

  public addState(id: string, state: MonsterState) {
    this.stateHash.add(id, state);
  }

  public getState(id: string) {
    return this.stateHash.get(id);
  }

  public deleteState(id: string) {
    this.stateHash.remove(id);
  }
}
