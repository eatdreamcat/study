const { ccclass, property } = cc._decorator;

/**
 * 所有场景的Script Component 都要继承这个
 * 包含基础的UI界面管理
 */
@ccclass
export default class BaseScene extends cc.Component {}
