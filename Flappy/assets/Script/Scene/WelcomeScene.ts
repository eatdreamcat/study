import BaseScene from "./BaseScene";
import { HashMap } from "../Utils/HashMap";
import { MatchEngine } from "../Matchvs/MatchEngine";
import { Config } from "../Config/Config";
import { TableManager } from "../Controller/TableManager";

const { ccclass, property } = cc._decorator;

/** 加载的步骤 */
export enum LOAD_STEP {
  /** 初始化 */
  READY = 2 << 0,
  /** 初始化 */
  INIT = 2 << 1,
  /** 注册 */
  REGISTER = 2 << 2,
  /** 登录 */
  LOGIN = 2 << 3,
  /** 匹配 */
  MATCH = 2 << 4,
  /** 加载表 */
  JSON_PARSE = 2 << 5,
  /** 动画播放完成 */
  ANIMATION_DONE = 2 << 6,
  /** 完成 */
  DONE = LOAD_STEP.READY |
    LOAD_STEP.INIT|
    LOAD_STEP.REGISTER |
    LOAD_STEP.LOGIN |
    LOAD_STEP.MATCH |
    LOAD_STEP.JSON_PARSE|
    LOAD_STEP.ANIMATION_DONE
}

@ccclass
export default class WelcomeScene extends BaseScene {
  /** 下一个场景名字 */
  @property({
    displayName: "游戏场景名",
    tooltip: "默认进入Game场景，如果需要指定场景，可以修改nextSceneName"
  })
  nextSceneName: string = "Game";

  /** 进入下一个场景前需要执行的callbacks */
  private _callbacks: HashMap<LOAD_STEP, Function> = new HashMap();
  /** 当前的步骤 */
  private _step: LOAD_STEP = LOAD_STEP.READY;
  private defaultAnimation: cc.Animation;
  onLoad() {
   
    this.defaultAnimation = this.node.getComponent(cc.Animation);
    if (this.defaultAnimation) {
      this.defaultAnimation.once(cc.Animation.EventType.FINISHED, this.animationDone, this);
    } else {
      this.nextStep(LOAD_STEP.ANIMATION_DONE);
      
    }

    if (Config.isMultiPlayer) {
      this.startMatch();
    } else {
      this.nextStep(LOAD_STEP.LOGIN);
      this.nextStep(LOAD_STEP.MATCH);
      this.nextStep(LOAD_STEP.REGISTER);
      this.nextStep(LOAD_STEP.INIT);
    }

    TableManager.inst.startLoad(function() {
      this.nextStep(LOAD_STEP.JSON_PARSE);
    }.bind(this));
  }

  animationDone() {
    this.defaultAnimation.off(cc.Animation.EventType.FINISHED);
    this.nextStep(LOAD_STEP.ANIMATION_DONE);
  }

  /************************联网游戏相关******start***************************** */
  /**
   * 多人游戏需要先注册登录进房间匹配
   */
  private startMatch() {
    // 绑定matchvs的执行环境
    MatchEngine.inst.uninit();
    MatchEngine.inst.bindContext(this);
    MatchEngine.inst.init(
      Config.channel,
      Config.platform,
      Config.GameID,
      Config.AppKey,
      Config.gameVersion
    );
  }

  /**
   * 初始化回调
   */
  public initResponse(status: number) {
    if (status == 200) {
      console.log("start register user");
      MatchEngine.inst.registerUser();
      this.nextStep(LOAD_STEP.INIT);
    } else {
    }
  }
  /**
   *注册回调
   */
  public registerUserResponse(userInfo: MsRegistRsp) {
    if (userInfo.status == 0) {
      MatchEngine.inst.login(userInfo.userID, userInfo.token, Config.DeviceID);
      this.nextStep(LOAD_STEP.REGISTER);
    } else {
    }
  }

  /**
   * 登录回调
   */
  public loginResponse(login: MsLoginRsp) {
    if (login.status == 200) {
      this.nextStep(LOAD_STEP.LOGIN);
      this.getRoomList();
    }
  }

  /** 获取房间列表 */
  private getRoomList() {
    let createRoomInfo = new MsCreateRoomInfo(
      "",
      Config.MaxPlayer,
      0,
      1,
      1,
      ""
    );

    MatchEngine.inst.getRoomListEx({
      maxPlayer: createRoomInfo.maxPlayer, //maxPlayer
      mode: createRoomInfo.mode, //mode
      canWatch: createRoomInfo.canWatch, //canWatch
      roomProperty: null, //roomProperty
      full: 0, //full 0-未满
      state: 1, //state 0-全部，1-开放 2-关闭
      sort: 2, //sort 0-不排序 1-创建时间排序 2-玩家数量排序 3-状态排序 都可以
      order: 0, //order 0-ASC  1-DESC 都可以
      pageNo: 0, //pageNo 从0开始 0为第一页
      pageSize: 10 //pageSize 每页数量 大于0
    });
  }

  public getRoomListExResponse(rsp: MsGetRoomListExRsp) {
    if (rsp.status == 200) {
      if (rsp.total <= 0) {
        this.createRoom();
      } else {
        MatchEngine.inst.joinRoom(rsp.roomAttrs[0].roomID, "", false);
      }
    }
  }

  public joinRoomResponse(
    status: number,
    roomUserInfoList: Array<MsRoomUserInfo>,
    roomInfo: MsRoomInfo
  ) {
    if (status == 200) {
      console.log(roomUserInfoList);
    } else {
      this.getRoomList();
    }
  }

  public joinRoomNotify(roomUserInfo: MsRoomUserInfo) {
    console.log(roomUserInfo);
  }

  private createRoom() {
    let createRoomInfo = new MsCreateRoomInfo(
      "",
      Config.MaxPlayer,
      0,
      1,
      1,
      ""
    );
    MatchEngine.inst.createRoom(createRoomInfo, "");
  }

  public createRoomResponse(rsp: MsCreateRoomRsp) {
    if (rsp.status == 200) {
    }
  }

  public joinOverNotify(notifyInfo: MsJoinOverNotifyInfo) {}

  public joinOverResponse(rsp: MsJoinOverRsp) {}

  /******************end******联网游戏相关**end********************************* */
  /** 注册每一步的回调 */
  public registerStep(
    step: LOAD_STEP,
    callback: (curStep: LOAD_STEP) => boolean
  ) {
    this._callbacks.add(step, callback);
  }

  /**
   * 下一步
   */
  private nextStep(loadStep: LOAD_STEP) {
    this._step |= loadStep;
    console.log("CUR STEP:" + LOAD_STEP[loadStep]);
    if (this._step >= LOAD_STEP.DONE) {
      cc.director.loadScene(this.nextSceneName);
    } else {
      if (loadStep == LOAD_STEP.ANIMATION_DONE) {
        if (this.defaultAnimation) {
          this._step ^= LOAD_STEP.ANIMATION_DONE;
          this.defaultAnimation.once(cc.Animation.EventType.FINISHED, this.animationDone, this);
          this.defaultAnimation.play();
        }
        
      }
    }
  }

  onDestroy() {
    MatchEngine.inst.unbindContext();
  }
}
