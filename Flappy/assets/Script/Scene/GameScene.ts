import BaseScene from "./BaseScene";
import { PhysicConf } from "../Config/Config";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameScene extends BaseScene {
  private physicManager = cc.director.getPhysicsManager();

  onLoad() {
    this.physicManager.enabled = PhysicConf.enabled;
    this.physicManager.gravity = PhysicConf.gravity;
    this.physicManager.debugDrawFlags |= PhysicConf.debugDrawFlags;
  }
}
