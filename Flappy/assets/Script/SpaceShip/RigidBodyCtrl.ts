import { GlobalEvent } from "../Event/EventName";
import { PhysicConf} from "../Config/Config";


const { ccclass, property } = cc._decorator;

@ccclass
export default class RigidBodyCtrl extends cc.RigidBody {
  // LIFE-CYCLE CALLBACKS:

  onLoad() {
    
  }

  start() {
    cc.director.on(GlobalEvent.JUMP, this.onTouch.bind(this), this);
    this.linearVelocity = PhysicConf.linearVelocity;
  }

  onTouch() {
    this.linearVelocity = PhysicConf.linearVelocity;
  }
  update (dt) {
    
  }
}
