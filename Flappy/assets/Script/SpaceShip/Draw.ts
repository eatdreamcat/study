// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Draw extends cc.Component {

    @property(cc.Graphics)
    drawPen: cc.Graphics = null;

    onLoad () {
        this.drawPen.moveTo(this.node.x, this.node.y);
    }

    start () {
        
    }

    lateUpdate () {
        this.drawPen.moveTo(this.node.x, this.node.y);
        this.node.x ++;
        this.drawPen.lineTo(this.node.x, this.node.y);
        this.drawPen.stroke();
        
        
    }
}
