import { GlobalEvent } from "../Event/EventName";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
/**
 * 主UI界面
 */
@ccclass
export default class MainUI extends cc.Component {
  @property(cc.ProgressBar)
  HpProgressBar: cc.ProgressBar = null;
  onLoad() {
    this.HpProgressBar.progress = 1;
    this.HpProgressBar.barSprite.node.color = cc.color(34, 73, 24);
    cc.director.on(
      GlobalEvent.MONSTER_ATTACKED,
      this.attacked.bind(this),
      this
    );
  }

  attacked(damage: number) {
    this.HpProgressBar.progress -= damage;
    if (this.HpProgressBar.progress > 0.4) {
      this.HpProgressBar.barSprite.node.color = cc.color(34, 73, 24);
    } else {
      this.HpProgressBar.barSprite.node.color = cc.color(185, 34, 8);
    }

    if (this.HpProgressBar.progress <= 0) {
      cc.director.emit(GlobalEvent.DEFEATED);
      cc.director.loadScene("Over", (err, overScene: cc.Scene) => {});
    }
  }

  start() {}

  update(dt) {}
}
